# Helium Registration
*Dynamic JSONSchema based data capturing for Helium*

## Project setup


### Prerequisites

* [nodejs](https://nodejs.org) v15.0.0


### Installation

```sh
npm install
cp default.env .env.local
```

In the new **.env.local** file:
```sh
VUE_APP_HE_REG_URL=https://registration.mezzanineapps.com/api
VUE_APP_HE_REG_CI=OBFUSCATED_OUTPUT_CLIENT_ID
VUE_APP_HE_REG_CS=OBFUSCATED_OUTPUT_CLIENT_SECRET
```

Replace **OBFUSCATED_OUTPUT_CLIENT_ID** with the output from this command:
```sh
# Substitute CLIENT_ID with the app client id
echo CLIENT_ID | gzip | Base64
```
And **OBFUSCATED_OUTPUT_CLIENT_SECRET** with the output from this command:
```sh
# Substitute CLIENT_SECRET with the app client secret
echo CLIENT_SECRET | gzip | Base64
```

To test either, run the obfuscated value through this command and compare the output with the initial value:
```sh
echo OBFUSCATED_VALUE | Base64 --decode | gunzip
```

### Tooling

[Visual Studio Code](https://code.visualstudio.com/) is recommended with the
following extensions:

* [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)
* [ENV](https://marketplace.visualstudio.com/items?itemName=IronGeek.vscode-env)
* [MDI Vuetify Intellisense](https://marketplace.visualstudio.com/items?itemName=Denifer.mdi-vuetify-intellisense)
* [Partial Diff](https://marketplace.visualstudio.com/items?itemName=ryu1kn.partial-diff)
* [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
* [Vue Peek](https://marketplace.visualstudio.com/items?itemName=dariofuzinato.vue-peek)
* [vuetify-vscode](https://marketplace.visualstudio.com/items?itemName=vuetifyjs.vuetify-vscode)

## Compile and hot-reload for development
```sh
npm run serve
```

## Compile and minify for production
```sh
npm run build
```

## Lints and fixes files
```sh
npm run lint
```

## Generate API documentation
```sh
npm run docs
```
