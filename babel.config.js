module.exports = {
  presets: [
    [
      "@babel/preset-env", {
        useBuiltIns: "entry",
        corejs: '3.3.5',
      }
    ],
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-export-default-from'
  ]
}
