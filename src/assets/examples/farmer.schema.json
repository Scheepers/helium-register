{
  "$id": "https://apps.mezzanineware/farmer.schema.json",
  "type": "object",
  "title": "Farmer",
  "$schema": "https://json-schema.org/draft/2019-09/schema#",
  "x-props": {
    "grow": true
  },
  "required": [
    "farmer",
    "farm"
  ],
  "x-display": "tabs",
  "properties": {
    "farm": {
      "type": "object",
      "title": "Farm",
      "required": [
        "locationId",
        "farmSize",
        "farmOwnership",
        "primaryCrop",
        "primaryCropSize",
        "termsAccepted"
      ],
      "he-weight": 1,
      "properties": {
        "farmSize": {
          "type": "number",
          "title": "Farm Size",
          "x-props": {
            "suffix": "Ha"
          },
          "he-weight": 9,
          "description": "The farmer's farm size (in Ha) within 1 decimal."
        },
        "products": {
          "type": "object",
          "oneOf": [
            {
              "title": "All",
              "required": [
                "marketsUsed"
              ],
              "he-weight": "0",
              "properties": {
                "marketsUsed": {
                  "$ref": "#/properties/farm/properties/products/definitions/marketUsed"
                },
                "productSold": {
                  "type": "string",
                  "const": "All"
                }
              }
            },
            {
              "title": "Some",
              "required": [
                "marketsUsed"
              ],
              "he-weight": "1",
              "properties": {
                "marketsUsed": {
                  "$ref": "#/properties/farm/properties/products/definitions/marketUsed"
                },
                "productSold": {
                  "type": "string",
                  "const": "Some"
                }
              }
            },
            {
              "title": "None",
              "he-weight": "2",
              "properties": {
                "productSold": {
                  "type": "string",
                  "const": "None"
                }
              }
            }
          ],
          "title": "Products Sold",
          "he-weight": 13,
          "definitions": {
            "marketUsed": {
              "enum": [
                "Neighbourhood or village",
                "Local shops",
                "Agri Parks or Hubs",
                "Fruit and Veg markets",
                "Supermarkets",
                "Livestock auction"
              ],
              "type": "string",
              "title": "Market",
              "he-weight": 14,
              "description": "Market where crops or livestock are sold."
            }
          }
        },
        "locationId": {
          "title": "Location",
          "type": "string",
          "he-weight": 8,
          "description": "The farmer's sub-district identifier."
        },
        "primaryCrop": {
          "enum": [
            "Vegetables",
            "Maize",
            "Dry beans",
            "Fruit",
            "Layer chickens",
            "Broiler chickens",
            "Goats",
            "Pigs",
            "Sheep",
            "Cattle"
          ],
          "type": "string",
          "title": "Primary Crop",
          "he-weight": 11,
          "description": "The most important crop or livestock of the farm."
        },
        "farmOwnership": {
          "enum": [
            "Owned",
            "Leased",
            "Communal land",
            "Other"
          ],
          "type": "string",
          "title": "Farm Ownership",
          "he-weight": 10,
          "description": "The state of ownership for the farm."
        },
        "termsAccepted": {
          "type": "boolean",
          "title": "I agree to the Terms & Conditions as they have been explained to me",
          "he-weight": 15
        },
        "primaryCropSize": {
          "type": "number",
          "title": "Primary Crop Size",
          "x-props": {
            "suffix": "Ha"
          },
          "he-weight": 12,
          "description": "The size (in Ha) occupied by this crop/livestock."
        }
      }
    },
    "farmer": {
      "type": "object",
      "title": "Farmer",
      "required": [
        "identification",
        "firstName",
        "lastName",
        "dateOfBirth",
        "gender",
        "mobileNumber"
      ],
      "he-weight": 0,
      "properties": {
        "gender": {
          "enum": [
            "male",
            "female",
            "other"
          ],
          "type": "string",
          "title": "Gender",
          "he-weight": 4
        },
        "lastName": {
          "type": "string",
          "title": "Last Name",
          "he-weight": 3,
          "minLength": 1,
          "x-options": {
            "messages": {
              "minLength": ""
            }
          }
        },
        "firstName": {
          "type": "string",
          "title": "First Name",
          "he-weight": 2,
          "minLength": 1,
          "x-options": {
            "messages": {
              "minLength": ""
            }
          }
        },
        "dateOfBirth": {
          "type": "string",
          "title": "Date of Birth",
          "format": "date",
          "he-weight": 1,
          "minLength": 1,
          "x-options": {
            "messages": {
              "minLength": ""
            }
          }
        },
        "mobileNumber": {
          "type": "string",
          "title": "Mobile Number",
          "he-weight": 5,
          "minLength": 1,
          "x-display": "custom-telephone",
          "x-options": {
            "messages": {
              "minLength": ""
            },
            "countries": [
              "ZA",
              "ZM"
            ]
          },
          "description": "The farmer's cellphone number"
        },
        "identification": {
          "type": "object",
          "oneOf": [
            {
              "title": "National ID",
              "required": [
                "nationalId"
              ],
              "he-weight": 1,
              "properties": {
                "nationalId": {
                  "type": "string",
                  "title": "National ID Number",
                  "pattern": "^[0-9]{13}$",
                  "x-props": {
                    "valid-keys": "[0-9]"
                  },
                  "x-display": "custom-textbox",
                  "x-options": {
                    "messages": {
                      "pattern": "This ID number is not valid."
                    }
                  },
                  "description": "The farmer's South African ID number."
                },
                "identificationType": {
                  "type": "string",
                  "const": "National ID"
                }
              }
            },
            {
              "title": "Passport",
              "required": [
                "passportNo"
              ],
              "he-weight": 0,
              "properties": {
                "passportNo": {
                  "type": "string",
                  "title": "Passport Number",
                  "he-weight": 6
                },
                "identificationType": {
                  "type": "string",
                  "const": "Passport"
                }
              }
            },
            {
              "title": "Birth Certificate",
              "he-weight": 2,
              "properties": {
                "identificationType": {
                  "type": "string",
                  "const": "Birth Certificate"
                }
              }
            },
            {
              "title": "Other",
              "required": [
                "otherId"
              ],
              "he-weight": 3,
              "properties": {
                "otherId": {
                  "type": "string",
                  "title": "Other identification information"
                },
                "identificationType": {
                  "type": "string",
                  "const": "Other"
                }
              }
            }
          ],
          "title": "Identification Type",
          "he-weight": 0
        }
      }
    }
  },
  "he-metadata": {
    "logo": "https://registration.mezzanineapps.com/img/mez_cf_logo.jpg",
    "title": "Farmer Registration",
    "submit": "Register",
    "subtitle": "Complete this form to register a farmer",
    "footer-links": [
      {
        "link": "https://bit.ly/35P6eMK",
        "value": "Terms & Conditions"
      },
      {
        "link": "https://connectedfarmerplatform.com/",
        "value": "Connected Farmer"
      }
    ],
    "footer-message": "This is a copyrighted system | All rights reserved"
  },
  "he-java-script-validator": "cf-farmer"
}