import Vue from 'vue'
import router from 'vue-router'
import vuetify from 'vuetify'
import VueTelInputVuetify from 'vue-tel-input-vuetify/lib';
import 'regenerator-runtime/runtime'

Vue.use(router)
Vue.use(vuetify)
Vue.use(VueTelInputVuetify, { vuetify })

Vue.component('vue-tel-input-vuetify', VueTelInputVuetify)