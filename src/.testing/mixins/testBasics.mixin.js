export default function (component) {

  test('Is a Vue instance',
    () => {
      expect(component.isVueInstance).toBeTruthy()
    }
  )

  test('Does not have a data function',
    () => {
      expect(typeof component.data).toBe('undefined')
    }
  )

  test('Has a props function',
    () => {
      expect(typeof component.props).toBe('function')
    }
  )

  test('Renders correctly',
    () => {
      expect(component.html()).toMatchSnapshot()
    }
  )
}
