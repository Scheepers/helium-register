fs = require('path')

module.exports = {
  // will resolve from test to snapshot path
  resolveSnapshotPath: (testPath, snapshotExtension) =>
    fs.dirname(testPath) +
    '/snapshots/' +
    fs.basename(testPath, '.test.js') +
    snapshotExtension,

  // will resolve from snapshot to test path
  resolveTestPath: (snapshotFilePath, snapshotExtension) =>
    fs.dirname(snapshotFilePath).replace('/snapshots', '/') +
    fs.basename(snapshotFilePath, '.snap') +
    '.test.js',

  // this is an example test path, used for preflight consistency check of the implementation above
  testPathForConsistencyCheck: 'tests/example.test.js',
}