workbox.core.setCacheNameDetails({ prefix: "helium-register" })

self.__precacheManifest = [].concat(self.__precacheManifest || [])
workbox.precaching.precacheAndRoute(self.__precacheManifest, {})

// ----------------------------- General Cacheing ------------------------------

// Route Navigation Requests to index.html
workbox.routing.registerNavigationRoute(
  workbox.precaching.getCacheKeyForURL("index.html")
)

// Cache Bootstrap
workbox.routing.registerRoute(
  /manifest\.json|index\.html|service\-worker\.js/,
  new workbox.strategies.CacheFirst()
)

// Cache Images
workbox.routing.registerRoute(
  /\.png|\.jpg|\.jpeg|\.bmp|\.ico|\.svg/,
  new workbox.strategies.CacheFirst()
)

// Cache Location
workbox.routing.registerRoute(
  /^https?:\/\/ip2c\.org\/s/,
  new workbox.strategies.CacheFirst()
)

// Cache API Definition GETs
workbox.routing.registerRoute(
  /^https?:\/\/registration\.mezzanineapps\.com\/api.+definition$/,
  new workbox.strategies.CacheFirst()
)

// Cache API Lookup GETs
workbox.routing.registerRoute(
  /^https?:\/\/registration\.mezzanineapps\.com\/api.+\/lookup\/.+/,
  new workbox.strategies.CacheFirst()
)

// Cache post requests
const bgSyncPlugin = new workbox.backgroundSync.Plugin("RegistrationQueue", {
  maxRetentionTime: 7 * 24 * 60, // Retry for max of 7 days
})

// Cache and sync POSTS
workbox.routing.registerRoute(
  /^https?:\/\/registration\.mezzanineapps\.com\/api/,
  new workbox.strategies.NetworkOnly({
    plugins: [bgSyncPlugin],
  }),
  "POST"
)
