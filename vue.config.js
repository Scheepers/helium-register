module.exports = {
  transpileDependencies: [
    "vuetify",
    "vue-tel-input-vuetify",
    "@koumoul/vjsf"
  ],

  configureWebpack: {
    devtool: "source-map"
  },

  pwa: {
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js',
    }
  }
}