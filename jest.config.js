module.exports = {

  transform: {
    '.*\\.(vue)$': 'vue-jest',
    '.*\\.(js)$': 'babel-jest',
  },
  transformIgnorePatterns: ["/node_modules/(?!vue-tel-input-vuetify|vuetify)"],

  testMatch: ['**/components/**/*.test.js'],
  moduleFileExtensions: ['js', 'json', 'vue'],
  moduleDirectories: [
    "node_modules",
    "src/components"
  ],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    "\\.(css|less|scss|sass)$": "identity-obj-proxy"
  },

  setupFiles: ["<rootDir>/src/testing/test.setup.js"],
  snapshotResolver: "<rootDir>/src/testing/snapshotResolver.js",

  collectCoverage: true,
  collectCoverageFrom: [
    '**/src/**/*.{js,vue}',
    '!**/node_modules/**',
    '!**/src/plugins/**',
    '!**/src/**/tests/**',
    '!**/src/router/**',
  ],
}
